﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP
{
    public static class ExplicitRandom
    {
        private static Random _instance = new Random();
        private static double NextDouble(double MinValue, double MaxValue)
        {
            return _instance.NextDouble() * (MaxValue - MinValue) + MinValue;
        }
        private static Line NextLine(double min, double max)
        {
            Point p1 = new Point(NextDouble(min,max),NextDouble(min,max));
            Point p2 = new Point(NextDouble(min, max), NextDouble(min, max));
            return new Line(p1, p2);
        }
        public static Line[] CreatePoints(int N,double min,double max)
        {
            Line[] lines = new Line[N];
            for (int i = 0; i < N; i++)
                lines[i] = NextLine(min,max);
            return lines;
        }
    }
}
