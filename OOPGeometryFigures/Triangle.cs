﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;

namespace OOP
{
   public sealed class Triangle:Polygon,ICloneable,IComparable<Triangle>
    {
        public double figureSquare
        {
            get;
            private set;
        }
 
       public Triangle(Line<Point> line1,Line<Point> line2,Line<Point> line3) : base(new Line<Point>[3] {
           line1,
           line2,
           line3
       })
       {
           figureSquare = Square();
       }

       public override double Square()
       {
           double half_p = figurePerimetr/2;
           return (Lines[0].lenght + Lines[1].lenght + Lines[2].lenght) / half_p;
       }

       public override string ToString()
       {
           return string.Format("Треугольник с периметром: {0}",Square());
       }

       public override bool Equals(object obj)
       {
           if (obj == null || obj.GetType() != GetType())
               return false;
           Triangle tri = (Triangle) obj;
           return figureSquare == tri.figureSquare;
       }

       public object Clone()
       {
           return MemberwiseClone();
       }

       public int CompareTo(Triangle t)
       {
           return figureSquare.CompareTo(t.figureSquare);
       }
       public override int GetHashCode()
       {
           return base.GetHashCode();
       }
    }
}
