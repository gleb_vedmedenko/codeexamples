﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP
{
    public struct Line<T> where T: struct 
    {
        public T start { get; private set; }
        public T end { get; private set; }
        public double lenght { get; private set; }
        public Line(T fromPoint, T toPoint)
            : this()
        {
            start = fromPoint;
            end = toPoint;
            Point l_start = (Point)Convert.ChangeType(start, typeof(Point));
            Point l_end = (Point)Convert.ChangeType(end, typeof(Point));
            lenght = Distance.Lenght(l_end, l_end);
        }
        public static Line<Point> operator +(Line<T> line1, Line<T> line2)
        {
            Point l1_start = (Point)Convert.ChangeType(line1.start, typeof(Point));
            Point l1_end = (Point)Convert.ChangeType(line1.end, typeof(Point));
            Point l2_start = (Point)Convert.ChangeType(line2.start, typeof(Point));
            Point l2_end = (Point)Convert.ChangeType(line2.end, typeof(Point));
            return new Line<Point>(l1_start + l2_start, l1_end + l2_end);
        }
        public static Line<Point> operator -(Line<T> line1, Line<T> line2)
        {
            Point l1_start = (Point)Convert.ChangeType(line1.start, typeof(Point));
            Point l1_end = (Point)Convert.ChangeType(line1.end, typeof(Point));
            Point l2_start = (Point)Convert.ChangeType(line2.start, typeof(Point));
            Point l2_end = (Point)Convert.ChangeType(line2.end, typeof(Point));
            return new Line<Point>(l1_start - l2_start, l1_end - l2_end);
        }

        public static bool operator ==(Line<T> line1, Line<T> line2)
        {
            Point l1_start = (Point)Convert.ChangeType(line1.start, typeof(Point));
            Point l1_end = (Point)Convert.ChangeType(line1.end, typeof(Point));
            Point l2_start = (Point)Convert.ChangeType(line2.start, typeof(Point));
            Point l2_end = (Point)Convert.ChangeType(line2.end, typeof(Point));
            if(l1_start == l2_start &&
                l1_end == l2_end)
                return true;
                return false;
        }
        public static bool operator !=(Line<T> line1, Line<T> line2)
        {
            Point l1_start = (Point)Convert.ChangeType(line1.start, typeof(Point));
            Point l1_end = (Point)Convert.ChangeType(line1.end, typeof(Point));
            Point l2_start = (Point)Convert.ChangeType(line2.start, typeof(Point));
            Point l2_end = (Point)Convert.ChangeType(line2.end, typeof(Point));
              if(l1_start != l2_start ||
                l1_end != l2_end)
                return true;
                return false;
              
        }
    }
}
