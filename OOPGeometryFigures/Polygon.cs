﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP
{
    public class Polygon :GeometryFigure, IComparable<Polygon>, ICloneable
    {
        public double figurePerimetr { 
            get; 
            private set;
        }
        public Polygon(Line[] lines) : base(lines)
        {
            figurePerimetr = Perimetr();
        }
        public int CompareTo(Polygon p)
        {
            return figurePerimetr.CompareTo(p.figurePerimetr);
        }
        public override double Square()
        {
            return 0;
        }
        public object Clone()
        {
            return MemberwiseClone();
        }
        public override double Perimetr()
        {
            return Lines.Sum(x => x.lenght);
        }
        public static Polygon operator +(Polygon source, Polygon other)
        {
            if(source.Lines.Length != other.Lines.Length)
                throw new ArgumentOutOfRangeException();
            var lines = new Line[other.Lines.Length];
            for (int i = 0; i < lines.Length; i++)
            {
                lines[i] = source.Lines[i] + other.Lines[i];
            }
            return new Polygon(lines);
        }
        public static Polygon operator -(Polygon source, Polygon other)
        {
            if (source.Lines.Length != other.Lines.Length)
                throw new ArgumentOutOfRangeException();
            var lines = new Line[other.Lines.Length];
            for (int i = 0; i < lines.Length; i++)
            {
                lines[i] = source.Lines[i] - other.Lines[i];
            }
            return new Polygon(lines);
        }
        public static bool operator ==(Polygon source, Polygon other)
        {
            if (source.Lines.Length != other.Lines.Length)
                throw new ArgumentOutOfRangeException(); 
            for (int i = 0; i < other.Lines.Length; i++)
            {
                if(source.Lines[i] == other.Lines[i])
                    continue;
                return false;
            }
            return true;
        }
        public static bool operator !=(Polygon source, Polygon other)
        {
            if (source.Lines.Length != other.Lines.Length)
                throw new ArgumentOutOfRangeException();
            for (int i = 0; i < other.Lines.Length; i++)
            {
                if (source.Lines[i] != other.Lines[i])
                    return true;
            }
            return false;
        }
        public static explicit operator Polygon(Elips elips)
        {
            var lines = new []{elips.Lines[0], elips.Lines[1]};

            return new Polygon(lines);
        }
    }
}
