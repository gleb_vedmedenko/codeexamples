﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP
{
   public class Hexagon:Polygon,IComparable<Hexagon>,ICloneable
   {
       public double figureSquare { get; private set; }

       public Hexagon(Line line1, Line line2, Line line3,
           Line line4, Line line5, Line line6)
           : base(new[]
           {
               line1,
               line2,
               line3,
               line4,
               line5,
               line6
           })
       {
           figureSquare = Square();
       }
       public int CompareTo(Hexagon hex)
       {
           return figureSquare.CompareTo(hex.figureSquare);
       }
       public object Clone()
       {
           return MemberwiseClone();
       }
       public override double Square()
       {
           return (Math.Sqrt(3)/4)*Math.Pow(Lines[0].lenght, 2);
       }
    }
}
