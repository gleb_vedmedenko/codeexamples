﻿using System;
using System.Globalization;
using System.Linq;

namespace OOP
{
    public sealed class Elips<T> : GeometryFigure,ICloneable,IComparable<Elips<T>>
        where T : struct 
    {
        public double figureSquare
        {
            get; 
            private set;
        }
        public double figurePerimetr
        {
            get; 
            private set;
        }
        public Elips(Line<Point> begin,Line<Point> end)
            : base(new []{begin,end})
        {
            figureSquare = Square();
            figurePerimetr = Perimetr();
        }
        public override double Square()
        {
            return Math.PI*Lines[0].lenght*Lines[1].lenght;
        }
        public override  double Perimetr()
        {
            return Lines.Sum(x =>x.lenght);
        }

        public int CompareTo(Elips<T> other)
        {
            return figureSquare.CompareTo(other.figureSquare);
        }

        public override string ToString()
        {
            return string.Format("Элипс с периметром {0} и площадью {1}", figurePerimetr, figureSquare);
        }

        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
                return false;
            Elips<T> p = (Elips<T>) obj;
            return figureSquare == p.figurePerimetr;
        }
        public object Clone()
        {
            return MemberwiseClone();
        }
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public static Elips<T> operator +(Elips<T> source,Elips<T> other)
        {
            return  new Elips<T>(source.Lines[0] + other.Lines[0],
                              source.Lines[1] + other.Lines[1]);
        }
        public static Elips<T> operator -(Elips<T> source, Elips<T> other)
        {
            return new Elips<T>(source.Lines[0] - other.Lines[0],
                              source.Lines[1] - other.Lines[1]);
        }
        public static bool operator ==(Elips<T> source, Elips<T> other)
        {
            
            if (source.Lines[0] == other.Lines[0]
                && source.Lines[1] == other.Lines[1])
                return true;
                return false;
        }
        public static bool operator !=(Elips<T> source, Elips<T> other)
        {
            if (source.Lines[0] != other.Lines[0]
                || source.Lines[1] != other.Lines[1])
                return true;
                return false;
        }
        public static explicit operator Elips<T>(Polygon polygon)
        {
            return new Elips<T>(polygon.Lines[0], polygon.Lines[1]);
        }

        

        
    }
}
