﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP
{
   public abstract class GeometryFigure
   {
       public Line<Point>[] Lines { get; set; }
       public int id { get; private set; }
       public GeometryFigure(params Line<Point>[] lines)
       {
           Lines = lines;
           id = ID.IncrementID();
       }

       public abstract double Square();
       public abstract double Perimetr();

   }
}
