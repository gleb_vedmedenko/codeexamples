﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP
{
    public struct Point : IComparable<Point>
    {
        public readonly double X;
        public readonly double Y;
        public readonly double lenght;
        public Point(double x, double y)
            : this()
        {
            X = x;
            Y = y;
            lenght = Lenght();
        }
        public int CompareTo(Point p)
        {
            return Lenght().CompareTo(p.lenght);

        }
        public double Lenght()
        {
            return Math.Sqrt(X * X + Y * Y);
        }

        public static Point operator +(Point p1, Point p2)
        {
            return  new Point(p1.X + p2.X, p2.Y + p2.Y);
        }
        public static Point operator -(Point p1, Point p2)
        {
            return new Point(p1.X - p2.X, p2.Y - p2.Y);
        }
        public static bool operator ==(Point p1,Point p2)
        {
            if (p1.X == p2.X && p1.Y == p2.Y)
                return true;
                return false;
        }
        public static bool operator !=(Point p1, Point p2)
        {
            if (p1.X != p2.X || p1.Y != p2.Y)
                return true;
                return false;
        }
        
    }
}                      
