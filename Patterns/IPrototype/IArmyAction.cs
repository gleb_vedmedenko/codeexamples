﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Patterns_AbstractFactory;

namespace Patterns_Preconditions
{
  public interface IArmyAction
  {
      IArmyAction Clone();
      void Raid(Point p);
      void Occupy(Point p);
      void BuildCamp();
      void MakeAmbush();
      void Hunt(Point p);
      void Hunt();
      void Move(Point p);
  }
}
