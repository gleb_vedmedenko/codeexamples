﻿
using System;
using System.Linq;

using Patterns_AbstractFactory;

namespace Patterns_Preconditions
{
   public class Player
   {
       private static Player _player;    
       public static Army humanArmy;
       public static Army OrcArmy;

       static Player()
       {
           humanArmy = new HumanArmy(new HArmyAction(), new HArmyCreator());
           OrcArmy = new OrcArmy(new OArmyAction(),new OArmyCreator());
       }
       private Player()
       {
       }

     
       public static Player getInstanse()
       {
           return _player ?? (_player = new Player());
       }
 

       public static void getInfo()
       {
           humanArmy.Creator.GetInfo();
           OrcArmy.Creator.GetInfo();
       }
   }
}
