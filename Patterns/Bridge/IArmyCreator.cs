﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using Patterns_AbstractFactory;

namespace Patterns_Preconditions
{
  public  interface IArmyCreator:IHumanCreator,IOrcCreator
  {     
      void AddHeroe(HeroesFactory factory);
      void GetInfo();
      double GetStats();
      double Power { get; }
  }
}
