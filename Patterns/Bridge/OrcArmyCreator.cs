﻿using System;
using System.Collections.Generic;

using Patterns_AbstractFactory;

namespace Patterns_Preconditions
{
    public class OrcArmyCreator
    {
        public OrcArmyBuilder builder { get; private set; }
        public OrcArmyCreator(OrcArmyBuilder builder,OrcArmy army)
      {
          this.builder = builder;
          this.builder.CreateArmy(army);
      }
      public IEnumerable<OrcUnit> GetUnits(int unitsCount)
      {
          builder.AddUnits(unitsCount);
          return builder.OrcArmy.units;
      }

      public IEnumerable<OrcHeroe> AddHeroe(HeroesFactory factory)
      {
          builder.AddHeroe(factory);
          return builder.OrcArmy.heroes;
      }
    }
}
