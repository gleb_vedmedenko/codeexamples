﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Patterns_AbstractFactory;

namespace Patterns_Preconditions
{
  public  interface IHumanCreator
    {
      void AddUnit(int count, HumanArmyBuilder builder);
    }

}
