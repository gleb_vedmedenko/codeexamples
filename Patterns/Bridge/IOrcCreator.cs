﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patterns_Preconditions
{
   public interface IOrcCreator
    {
       void AddUnit(int count, OrcArmyBuilder type);
    }
}
