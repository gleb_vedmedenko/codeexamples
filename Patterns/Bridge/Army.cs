﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patterns_Preconditions
{
  public abstract  class Army
  {
      protected IArmyAction armyAction;
      protected IArmyCreator armyCreator;
      
      public IArmyAction Action
      {
        get { return armyAction; }  set { armyAction = value; }
      }

      public IArmyCreator Creator
      {
          get { return armyCreator; } set { armyCreator = value; }
      }
      protected Army(IArmyAction armyActionObj, IArmyCreator armyCreatorObj)
      {
          armyAction = armyActionObj;
          armyCreator = armyCreatorObj;
      }

      protected Army()
      {
          
      }

    

  }
}
