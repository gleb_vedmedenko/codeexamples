﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Patterns_AbstractFactory;

namespace Patterns_Preconditions
{
  public class HumanArmyCreator
    {
      public HumanArmyBuilder builder { get; private set; }
      public HumanArmyCreator(HumanArmyBuilder builder,HumanArmy army)
      {
          this.builder = builder;
          this.builder.CreateArmy(army);
      }
      public IEnumerable<HumanUnit> GetUnits(int unitsCount)
      {
          builder.AddUnits(unitsCount);
          return builder.humanArmy.units;
      }

      public IEnumerable<HumanHeroe> GetHeroe(HeroesFactory factory)
      {
          builder.AddHeroe(factory);
          return builder.humanArmy.heroes;
      }

    }
}
