﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Patterns_AbstractFactory;

namespace Patterns_Preconditions
{
  public  class HArmyAction: IArmyAction
  {

      public HumanArmy army;

     
      public IArmyAction Clone()
      {
          return MemberwiseClone() as IArmyAction;
      }

      public void BuildCamp()
      {
          
          Console.WriteLine("CampBuilded");
      }

      public void Raid(Point p)
      {
          Console.WriteLine("Raid target");
      }

      public void Occupy(Point p)
      {
          Console.WriteLine("Occupyted");
      }

      public void Hunt(Point p)
      {
          Console.WriteLine("Hunted");
      }
      public void Hunt()
      {
          Console.WriteLine("Hunted");
      }
      public void MakeAmbush()
      {
          Console.WriteLine("Ambush");
      }

      public void Move(Point p)
      {
          Console.WriteLine("Move to point");
      }
    }
}
