﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using Patterns_AbstractFactory;

namespace Patterns_Preconditions
{
    [Serializable]
  public  class HArmyCreator:IArmyCreator
    {
      public HumanArmy army;
      public double Power { get; private set; }
      public void AddUnit(int count, HumanArmyBuilder builder)
      {

          var creator = new HumanArmyCreator(builder,army);
          creator.GetUnits(count);
      }

      public void AddHeroe(HeroesFactory factory)
      {
          army.heroes.Add(new HumanHeroe(factory));
      }
      public object DeepCopy()
      {
          object army = null;
          using (var tempStream = new MemoryStream())
          {
              var binForm = new BinaryFormatter(null, new StreamingContext(StreamingContextStates.Clone));
              binForm.Serialize(tempStream, this);
              tempStream.Seek(0, SeekOrigin.Begin);

              army = binForm.Deserialize(tempStream);
          }
          return army;
      }
      public void AddUnit(int count, OrcArmyBuilder factory)
      {
          throw new NotImplementedException();
      }

      public void GetInfo()
      {
              foreach (var units in army.units)
              {
                  Console.WriteLine("Race:\t{0}\t AttackRate\t{1}", units.mainParams.Race, units.mainParams.AttackRate);
              }   
      }

        public double GetStats()
        {
            double stats = 0;
            foreach (var humanUnit in army.units)
            {
                stats += humanUnit.getStats();
            }
            Power = stats;
            return stats;
        }
    }
}
