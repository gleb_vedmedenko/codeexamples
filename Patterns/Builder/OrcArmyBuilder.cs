﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patterns_Preconditions
{
  public abstract  class OrcArmyBuilder
    {
        public OrcArmy OrcArmy { get; private set; }

        public void CreateArmy(OrcArmy army)
        {
            OrcArmy = army;
        }

        public abstract void AddUnits(int count, OrcFactory factory);
        public abstract void AddUnits(int count);
        public abstract void AddHeroe(HeroesFactory factory);
    }
    
}
