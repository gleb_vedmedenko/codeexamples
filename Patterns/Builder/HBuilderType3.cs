﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Patterns_AbstractFactory;

namespace Patterns_Preconditions
{
    class HBuilderType3:HumanArmyBuilder
    {
        public override void AddUnits(int count)
        {
            this.humanArmy.AddHumanUnit(count, new HumanCavalry());
            this.humanArmy.AddHumanUnit(count, new HumanInfantry());
            this.humanArmy.AddHumanUnit(count, new HumanArcher());
        }

        public override void AddUnits(int count, HumanFactory factory)
        {
            this.humanArmy.AddHumanUnit(count, factory);
        }

        public override void AddHeroe(HeroesFactory factory)
        {
            this.humanArmy.AddHeroe(factory);
        }
    }
}
