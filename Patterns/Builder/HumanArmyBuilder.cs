﻿using System;
using Patterns_AbstractFactory;

namespace Patterns_Preconditions
{
    public abstract class HumanArmyBuilder
    {
    public HumanArmy humanArmy { get; private set; }

        public void CreateArmy(HumanArmy army)
        {
            humanArmy = army;
        }

        public abstract void AddUnits(int count, HumanFactory factory);
        public abstract void AddUnits(int count);
        public abstract void AddHeroe(HeroesFactory factory);
    }
}
