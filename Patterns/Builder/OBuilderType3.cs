﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patterns_Preconditions
{
   public class OBuilderType3: OrcArmyBuilder
    {
        public override void AddUnits(int count)
        {
            this.OrcArmy.AddOrcUnit(count, new OrcArcher());
            this.OrcArmy.AddOrcUnit(count, new OrcCavalry());
            this.OrcArmy.AddOrcUnit(count, new OrcInfantry());;
        }

        public override void AddUnits(int count, OrcFactory factory)
        {
            this.OrcArmy.AddOrcUnit(count, factory);
        }

        public override void AddHeroe(HeroesFactory factory)
        {
            this.OrcArmy.AddHeroe(factory);
        }
    }
}
