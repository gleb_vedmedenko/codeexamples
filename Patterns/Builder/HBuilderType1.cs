﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Patterns_AbstractFactory;

namespace Patterns_Preconditions
{
   public class HBuilderType1: HumanArmyBuilder
    {
       public override void AddUnits(int count)
       {
           this.humanArmy.AddHumanUnit(count,new HumanArcher());
           this.humanArmy.AddHumanUnit(count, new HumanInfantry());

       }

       public override void AddUnits(int count, HumanFactory factory)
       {
           this.humanArmy.AddHumanUnit(count, factory);
       }

       public override void AddHeroe(HeroesFactory factory)
       {
           this.humanArmy.AddHeroe(factory);
       }
    }
}
