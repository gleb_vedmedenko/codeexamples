﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Patterns_AbstractFactory;

namespace Patterns_Preconditions
{
  public  class King
    {
      public FacadeAction orders { get;private set; }
      public King(FacadeAction facade)
      {
          orders = facade;
      }

      public void TotalMoving(Point p)
      {
          for (int i = 0; i < orders.army.Length; i++)
          {
              orders.army[i].Action.Move(p);
          }
      }

      public void Defent(Point p)
      {
          for (int i = 0; i < orders.army.Length; i++)
          {
              if (i == 0)
              {
                  orders.army[i].Action.BuildCamp();
                  orders.army[i].Action.Move(p);
                  continue;
              }                  
              orders.army[i].Action.Move(p);
              orders.army[i].Action.MakeAmbush();
          }
      }

      public void TotalHunting()
      {
          for (int i = 0; i < orders.army.Length; i++)
          {
              orders.army[i].Action.Hunt();
          }
      }

      public void TotalOccupy(Point p)
      {
          var strongestArmy = getStrongestArmy();
          for (int i = 0; i < orders.army.Length; i++)
          {
              if (strongestArmy.Equals(orders.army[i]))
              {
                  Console.WriteLine("Strongest occupyed");
                  orders.army[i].Action.Move(p);  
                  orders.army[i].Action.Occupy(p);
                  continue;
              }
              orders.army[i].Action.Move(p);             
              orders.army[i].Action.BuildCamp();
          }
      }

      private Army getStrongestArmy()
      {
          var max = orders.army.Max(x => x.Creator.GetStats());
          var army = orders.army.FirstOrDefault(x => x.Creator.Power == max);
          return army;
      }
    }
}
