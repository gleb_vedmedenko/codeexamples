﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patterns_Preconditions
{
  public class General
    {
      public ArmyDecorator decorator { get; private set; }
      public General(ArmyDecorator decorator)
      {
          this.decorator = decorator;
      }

    }
}
