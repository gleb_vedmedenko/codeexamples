﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Patterns_AbstractFactory;

namespace Patterns_Preconditions
{
 public  class ArmyDecorator
 {
      private Army army;
      public ArmyDecorator(Army _army)
      {
          army = _army;
      }

     public void Move(Point p)
     {
        army.Action.Move(p);
        army.Action.Hunt(p);
     }

     public void Raid(Point p)
     {
         army.Action.Raid(p);
         army.Action.MakeAmbush();
     }

     public void Occupy(Point p)
     {
         army.Action.Occupy(p);
         army.Action.BuildCamp();
     }

     public void BuildCamp()
     {
         army.Action.BuildCamp();
         army.Action.Hunt();
     }

     public void MakeAmbush()
     {
         army.Action.MakeAmbush();
         army.Action.Hunt();
     }

     public void Hunt(Point p)
     {
         army.Action.Hunt();
         army.Action.BuildCamp();
     }
  }
}
