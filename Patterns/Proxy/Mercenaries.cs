﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;
using Patterns_AbstractFactory;

namespace Patterns_Preconditions
{
  public class Mercenaries
  {
      private Army army;
      public Point position { get; set; }
      public int Count { get; private set; }
      public bool Humans { get; private set; }
            public Mercenaries(int count, bool humans)
      {
          Count = count;
          Humans = humans;
       
      }

      private Army isHuman(bool human)
      {
          if(human)
          return army = new HumanArmy(new HArmyAction(), new HArmyCreator());
          return   army = new OrcArmy(new OArmyAction(), new OArmyCreator());
          
      }

      public void Move(Point p)
      {
          position = p;
      }

      public void BuildCamp()
      {
          Console.WriteLine("Camp builded");
      }

      public void MakeAmbush()
      {
          Console.WriteLine("Ambush");
      }

      public void Hunt(Point p)
      {
          position = p;
      }

      public void Raid(Point p)
      {
      
         if(army == null)
            army = isHuman(Humans);
          if(Humans)
          army.Creator.AddUnit(Count,new HBuilderType1() );
          else
          army.Creator.AddUnit(Count,new OBuilderType1());
          army.Action.Raid(p);
      }

      public void Occupy(Point p)
      {
         
          if (army == null)
              army = isHuman(Humans);
          if (Humans)
              army.Creator.AddUnit(Count, new HBuilderType1());
          else
              army.Creator.AddUnit(Count, new OBuilderType1());
          army.Action.Occupy(p);
      }

      public void GetInfo()
      {
          if (army != null)
          army.Creator.GetInfo();
          else
          {
              throw new Exception("Empty army");
          }
      }
    }
}
