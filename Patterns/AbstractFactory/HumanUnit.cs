﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Patterns_Preconditions;

namespace Patterns_AbstractFactory
{
    [Serializable]
  public class HumanUnit:Unit
  {
      public readonly HumanParams parameters;

      public HumanUnit(HumanFactory factory)
      {
          parameters = factory.CreateHumanUnit();
          mainParams = parameters.baseParams;
      }

      public override void Move(Point p)
      {
          parameters.Move(p);
      }

      public override void Attack(Unit enemy)
      {
          parameters.Attack(enemy);
      }

      public override void Defend()
      {
          parameters.Defend();
      }
      public override void Charge(Unit enemy)
      {
          parameters.Charge(enemy);
      }
      public override void Run(Point position)
      {
          parameters.Run(position);
      }

      public override void Shoot(Unit enemy)
      {
          parameters.Shoot(enemy);
      }

        public override double getStats()
        {
            double d = parameters.baseParams.AttackRate + parameters.baseParams.DefenceRate
                + parameters.baseParams.Health + parameters.baseParams.Speed;
            return d;
        }

        public override void FalseAttack(Unit enemy)
      {
          throw new NotImplementedException();
      }

      public override void FastAttack(Unit enenmy)
      {
          throw new NotImplementedException();
      }

      public override void RageAttack(Unit enemy)
      {
          throw new NotImplementedException();
      }

      
  }
}
