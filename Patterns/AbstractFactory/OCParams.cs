﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Patterns_AbstractFactory;

namespace Patterns_Preconditions
{
    [Serializable]
   public sealed class OCParams:OrcParams
    {
       public OCParams(Point position, int health,
           int attackRate, int speed, int defenceRate,Race race):
           base(position,health,attackRate,speed,defenceRate,race)
       {
           
       }
       public override void Run(Point p)
       {
           Console.WriteLine("Run to\t{0}", p);
           baseParams.Position = p;
       }
       public override void RageAttack(Unit enemy)
       {
           if (enemy.mainParams.Race == baseParams.Race)
               return;
           enemy.mainParams.Health -= baseParams.AttackRate * 2;
       }
       public override void FalseAttack(Unit enemy)
       {
           if (enemy.mainParams.Race == baseParams.Race)
               return;
           Console.WriteLine("False Attack");
       }
       public override void Attack(Unit enemy)
       {
           throw new NotImplementedException();
       }

       public override void Shoot(Unit enemy)
       {
           throw new NotImplementedException();
       }

       public override void FastAttack(Unit enemy)
       {
           throw new NotImplementedException();
       }
    }
}
