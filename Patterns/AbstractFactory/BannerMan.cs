﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patterns_Preconditions
{
  public  class BannerMan:HeroesFactory
    {
      public override HeroParams CreateHeroe()
      {
          return new HumanHeroes(HeroeType.BannerMan);
      }
    }
}
