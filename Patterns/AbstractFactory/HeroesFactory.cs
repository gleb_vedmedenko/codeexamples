﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patterns_Preconditions
{
  public abstract  class HeroesFactory
  {
      public abstract HeroParams CreateHeroe();
  }
}
