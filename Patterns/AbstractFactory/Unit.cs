﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Patterns_AbstractFactory;

namespace Patterns_Preconditions
{
    [Serializable]
   public abstract class Unit
   {
       public  BaseParams mainParams { get; set; }
       public abstract void Move(Point position);
       public abstract void Attack(Unit enemy);
       public abstract void Defend();
       public abstract void Run(Point position);
       public abstract void Shoot(Unit enemy);
       public abstract void Charge(Unit enemy);
       public abstract void FastAttack(Unit enenmy);
       public abstract void RageAttack(Unit enemy);
       public abstract void FalseAttack(Unit enemy);
       public abstract double getStats();
   }
}
