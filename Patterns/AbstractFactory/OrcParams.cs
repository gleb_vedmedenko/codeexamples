﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Patterns_AbstractFactory;

namespace Patterns_Preconditions
{
    [Serializable]
  public abstract class OrcParams
    {
       public readonly BaseParams baseParams;
       public abstract void Run(Point p);
       public abstract void Attack(Unit enemy);
       public abstract void RageAttack(Unit enemy);
       public abstract void FalseAttack(Unit enemy);
       public abstract void FastAttack(Unit enemy);
       public abstract void Shoot(Unit enemy);
             
       protected OrcParams(Point position, int health,
           int attackRate, int speed, int defenceRate,Race race)
       {
           baseParams = new BaseParams(position, health, attackRate, speed
               , defenceRate, race);
                 
       }
    }
}
