﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patterns_AbstractFactory
{
  public class HumanArcher:HumanFactory
    {
      public override HumanParams CreateHumanUnit()
      {
          return new HAParams(new Point(0,0),100,50,20,5,Race.Human);
      }
    }
}
