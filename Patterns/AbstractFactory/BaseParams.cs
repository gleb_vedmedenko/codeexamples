﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Patterns_AbstractFactory;

namespace Patterns_Preconditions
{
    [Serializable]
   public  class BaseParams
    {
       public Point Position { get; set; }
       public int Health { get; set; }
       public int AttackRate { get; set; }
       public int Speed { get; set; }
       public int DefenceRate { get; set; }
    
       public Race Race { get; set; }

        public BaseParams(Point position, int health,
           int attackRate, int speed, int defenceRate,Race race,bool shot = false,bool rage = false)
       {
           Position = position;
           Health = health;
           AttackRate = attackRate;
           Speed = speed;
           DefenceRate = defenceRate;
           Race = race;
           
       }
    }
}
