﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Patterns_AbstractFactory;

namespace Patterns_Preconditions
{
    [Serializable]
   public class OrcHeroes:HeroParams
    {
       public OrcHeroes(HeroeType heroeType) : base(heroeType)
        {

        }

        public override void OrcHeroeType(IEnumerable<OrcUnit> units)
        {
            switch (HeroeType)
            {
                case HeroeType.Berserk:
                    var infantry = units.Where(x => x.parameters is OIParams);
                    BaffInfantry(infantry.ToArray());
                    break;
                case HeroeType.Chieftain:
                    var archers = units.Where(x => x.parameters is OAParams);
                    BaffArcher(archers.ToArray());
                    break;
                case HeroeType.Ogre:
                    var cavalry = units.Where(x => x.parameters is OCParams);
                    BaffCavalry(cavalry.ToArray());
                    break;
            }
        }

        private void BaffInfantry(OrcUnit[] humanUnitses)
        {
            for (var i = 0; i < humanUnitses.Length; i++)
            {
                humanUnitses[i].mainParams.AttackRate += 100;
                humanUnitses[i].mainParams.DefenceRate += 50;
                
            }
        }

        private void BaffArcher(OrcUnit[] humanUnitses)
        {
            for (var i = 0; i < humanUnitses.Length; i++)
            {
                humanUnitses[i].mainParams.AttackRate += 100;
                humanUnitses[i].mainParams.Health += 100;
            }
        }

        private void BaffCavalry(OrcUnit[] humanUnitses)
        {
            for (var i = 0; i < humanUnitses.Length; i++)
            {
                humanUnitses[i].mainParams.AttackRate += 100;
            }
        }

       public override void HumanHeroeType(IEnumerable<HumanUnit> unit)
       {
           throw new NotImplementedException();
       }
    }
}
