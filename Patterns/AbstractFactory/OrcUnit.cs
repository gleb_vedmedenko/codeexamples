﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Patterns_AbstractFactory;

namespace Patterns_Preconditions
{
    [Serializable]
   public class OrcUnit:Unit
    {
        public readonly OrcParams parameters;

      public OrcUnit(OrcFactory factory)
      {
          parameters = factory.CreateOrcUnit();
          mainParams = parameters.baseParams;
      }

      public override void Attack(Unit enemy)
      {
          parameters.Attack(enemy);
      }    
      public override void Run(Point position)
      {
          parameters.Run(position);
      }

      public override void Shoot(Unit enemy)
      {
          parameters.Shoot(enemy);
      }
      public override void FalseAttack(Unit enemy)
      {
          parameters.FalseAttack(enemy);
      }

      public override void FastAttack(Unit enenmy)
      {
          parameters.FastAttack(enenmy);
      }

      public override void RageAttack(Unit enemy)
      {
          parameters.RageAttack(enemy);
      }
      public override double getStats()
      {
          double d = parameters.baseParams.AttackRate + parameters.baseParams.DefenceRate
              + parameters.baseParams.Health + parameters.baseParams.Speed;
          return d;
      }
      public override void Defend()
      {
          throw new NotImplementedException();
      }
      public override void Charge(Unit enemy)
      {
          throw new NotImplementedException();
      }
      public override void Move(Point p)
      {
          throw new NotImplementedException();
      }
    }
}
