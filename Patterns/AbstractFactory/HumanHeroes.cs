﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Patterns_AbstractFactory;

namespace Patterns_Preconditions
{
    [Serializable]
    public class HumanHeroes : HeroParams
    {
        public HumanHeroes(HeroeType heroeType) : base(heroeType)
        {

        }

        public override void HumanHeroeType(IEnumerable<HumanUnit> units)
        {
            switch (HeroeType)
            {
                case HeroeType.BannerMan:
                    var infantry = units.Where(x => x.parameters is HIParams);
                    BaffInfantry(infantry.ToArray());
                    break;
                case HeroeType.Commander:
                    var archers = units.Where(x => x.parameters is HAParams);
                    BaffArcher(archers.ToArray());
                    break;
                case HeroeType.Knight:
                    var cavalry = units.Where(x => x.parameters is HCParams);
                    BaffCavalry(cavalry.ToArray());
                    break;
            }
        }

        private void BaffInfantry(HumanUnit[] humanUnitses)
        {
            for (var i = 0; i < humanUnitses.Length; i++)
            {
                humanUnitses[i].mainParams.AttackRate += 100;
                humanUnitses[i].mainParams.DefenceRate += 50;
                
            }
        }

        private void BaffArcher(HumanUnit[] humanUnitses)
        {
            for (var i = 0; i < humanUnitses.Length; i++)
            {
                humanUnitses[i].mainParams.AttackRate += 100;
                humanUnitses[i].mainParams.Health += 100;
            }
        }

        private void BaffCavalry(HumanUnit[] humanUnitses)
        {
            for (var i = 0; i < humanUnitses.Length; i++)
            {
                humanUnitses[i].mainParams.AttackRate += 100;
            }
        }

        public override void OrcHeroeType(IEnumerable<OrcUnit> unit)
        {
            throw new NotImplementedException();
        }
    }
}
