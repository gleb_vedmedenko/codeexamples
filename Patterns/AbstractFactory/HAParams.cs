﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Patterns_Preconditions;

namespace Patterns_AbstractFactory
{
    [Serializable]
   public sealed class HAParams:HumanParams
    {
       public HAParams(Point position, int health,
           int attackRate, int speed, int defenceRate,Race race):
           base(position,health,attackRate,speed,defenceRate,race)
       {
           
       }

       public override void Attack(Unit enemy,bool isRage = false, bool isShot = false)
       {
           if (enemy.mainParams.Race== baseParams.Race) 
               return;
           enemy.mainParams.Health -= baseParams.AttackRate/2;
       }

       public override void Move(Point p)
       {
           Console.WriteLine("Move to\t{0}",p);
           baseParams.Position = p;
       }

       public override void Defend()
       {
           Console.WriteLine("Defend {0}", baseParams.DefenceRate);
       }

       public override void Shoot(Unit enemy)
       {
           if (enemy.mainParams.Race == baseParams.Race)
               return;
           enemy.mainParams.Health -= baseParams.AttackRate;
       }
       public override void Charge(Unit enemy)
       {
           throw new NotImplementedException();
       }

       public override void Run(Point p)
       {
           throw new NotImplementedException();
       }

       
    }
}
