﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Patterns_AbstractFactory;

namespace Patterns_Preconditions
{
    [Serializable]
   public  class HumanHeroe
   {
       public readonly HeroParams heroes;

       public HumanHeroe(HeroesFactory factory)
       {
          heroes = factory.CreateHeroe();
       }

       public void BaffArmy(HumanUnit[] humanUnitses)
       {       
           heroes.HumanHeroeType(humanUnitses);
       }
   }
}
