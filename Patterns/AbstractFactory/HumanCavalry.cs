﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Patterns_AbstractFactory;

namespace Patterns_Preconditions
{
   public class HumanCavalry:HumanFactory
    {    
       public override HumanParams CreateHumanUnit()
       {
           return new HCParams(new Point(0, 0), 100, 50, 20, 5, Race.Human);
       }
    }
}
