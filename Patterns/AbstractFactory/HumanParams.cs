﻿
using System;
using Patterns_Preconditions;

namespace Patterns_AbstractFactory
{
    public enum Race
    {
        Human, ORc
    }
    [Serializable]
   public abstract class HumanParams
    {
       public readonly BaseParams baseParams;
       public abstract void Move(Point p);
       public abstract void Attack(Unit enemy,bool isRage = false, bool isShot = false);
       public abstract void Defend();
       public abstract void Shoot(Unit enemy);
       public abstract void Run(Point p);
       public abstract void Charge(Unit enemy);

       protected HumanParams(Point position, int health,
           int attackRate, int speed, int defenceRate,Race race)
       {
           baseParams = new BaseParams(position, health, attackRate, speed
               , defenceRate, race);
                 
       }
    }
}
