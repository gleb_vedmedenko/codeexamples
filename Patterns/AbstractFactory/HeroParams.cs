﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Patterns_AbstractFactory;

namespace Patterns_Preconditions
{
    public enum HeroeType
    {
        BannerMan,
        Commander,
        Knight,
        Berserk,
        Chieftain,
        Ogre
    }
    [Serializable]
   public abstract class HeroParams
    {
       public HeroeType HeroeType { get; set; }

       protected HeroParams(HeroeType heroeType)
       {
           HeroeType = heroeType;
       }

       public abstract void OrcHeroeType(IEnumerable<OrcUnit> unit);
       public abstract void HumanHeroeType(IEnumerable<HumanUnit> unit);
    }
}
