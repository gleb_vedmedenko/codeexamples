﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;

namespace Patterns_Preconditions
{
   public  class Berserk: HeroesFactory
    {
       public override HeroParams CreateHeroe()
       {
           return new OrcHeroes(HeroeType.Berserk);
       }
    }
}
