﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Patterns_AbstractFactory;

namespace Patterns_Preconditions
{
   public class OrcCavalry:OrcFactory
    {
       public override OrcParams CreateOrcUnit()
       {
           return new OCParams(new Point(0, 0), 100, 50, 20, 5, Race.ORc);
       }
    }
}
