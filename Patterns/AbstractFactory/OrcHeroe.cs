﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Patterns_AbstractFactory;

namespace Patterns_Preconditions
{
    [Serializable]
   public class OrcHeroe
    {
        public readonly HeroParams heroes;

        public OrcHeroe(HeroesFactory factory)
       {
          heroes = factory.CreateHeroe();
       }

       public void BaffArmy(OrcUnit[] orcUnits)
       {       
           heroes.OrcHeroeType(orcUnits);
       }
    }
}
