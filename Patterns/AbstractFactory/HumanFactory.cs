﻿using Patterns_Preconditions;

namespace Patterns_AbstractFactory
{
  public abstract class HumanFactory
  {
      public abstract HumanParams CreateHumanUnit();
 
  }
}
