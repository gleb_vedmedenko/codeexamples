﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Patterns_AbstractFactory;

namespace Patterns_Preconditions
{
    [Serializable]
   public sealed class HIParams:HumanParams
    {

       public HIParams(Point position, int health,
           int attackRate, int speed, int defenceRate,Race race):
           base(position,health,attackRate,speed,defenceRate,race)
       {
           
       }

       public override void Attack(Unit enemy, bool isRage = false, bool isShot = false)
       {
           if (enemy.mainParams.Race == baseParams.Race) 
               return;
           enemy.mainParams.Health -= baseParams.AttackRate;
           Console.WriteLine("Attack \t{0}", baseParams.AttackRate);
       }

       public override void Move(Point p)
       {
           Console.WriteLine("Got to\t{0}", p);
       }

       public void Def()
       {
           Console.WriteLine("Def");
       }
       public override void Defend()
       {
           Console.WriteLine("Defense \t{0}", baseParams.DefenceRate);
       }

       public override void Shoot(Unit enemy)
       {
           throw new NotImplementedException();
       }

       public override void Run(Point p)
       {
           throw new NotImplementedException();
       }

       public override void Charge(Unit enemy)
       {
           throw new NotImplementedException();
       }
    }
}
