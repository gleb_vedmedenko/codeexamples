﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patterns_Preconditions
{
   public  class Ogre:HeroesFactory
    {
       public override HeroParams CreateHeroe()
       {
           return new OrcHeroes(HeroeType.Ogre);
       }
    }
}
