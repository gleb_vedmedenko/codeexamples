﻿using System;
using Patterns_AbstractFactory;

namespace Patterns_Preconditions
{
    [Serializable]
   public sealed class HCParams:HumanParams
    {
        public HCParams(Point position, int health,
           int attackRate, int speed, int defenceRate,Race race):
           base(position,health,attackRate,speed,defenceRate,race)
       {
           
       }

        public override void Attack(Unit enemy, bool isRage = false, bool isShot = false)
        {
            if (enemy.mainParams.Race != baseParams.Race)
                Console.WriteLine("Attack \t{0}", baseParams.AttackRate);                 
        }

       public override void Move(Point p)
       {
           Console.WriteLine("Move to\t{0}",p);
       }

       public override void Charge(Unit enemy)
       {
           if (enemy.mainParams.Race != baseParams.Race)
               Console.WriteLine("Charge Attack \t{0}", baseParams.AttackRate);          
       }

       public override void Run(Point p)
       {
           baseParams.Position = p;
           Console.WriteLine("Run to\t{0}", p);
       }

       public override void Shoot(Unit enemy)
       {
           throw new NotImplementedException();
       }

       public override void Defend()
       {
           throw new NotImplementedException();
       }
    }
}
