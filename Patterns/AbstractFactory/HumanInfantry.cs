﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Patterns_AbstractFactory;

namespace Patterns_Preconditions
{
   public class HumanInfantry: HumanFactory
   {
      
       public override HumanParams CreateHumanUnit()
       {
           return new HIParams(new Point(0, 0),100, 75, 20, 5, Race.Human);
       }
       
    }
}
