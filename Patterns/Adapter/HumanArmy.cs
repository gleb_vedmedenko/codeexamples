﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using Patterns_AbstractFactory;

namespace Patterns_Preconditions
{
    [Serializable]
   public class HumanArmy:Army,IHBuffer
   {
       public List<HumanUnit> units { get; private set; }
       public List<HumanHeroe> heroes { get; private set; }

        
        public HumanArmy(HArmyAction action,HArmyCreator creator):base(action,creator)
       {
           units = new List<HumanUnit>();
           heroes = new List<HumanHeroe>();
           creator.army = this;
           action.army = this;
       }

       public void AddHumanUnit(int count, HumanFactory factory)
       {

           var army = new HumanUnit[count];
           for (int i = 0; i < count; i++)
           {
               army[i] = new HumanUnit(factory);
           }
           units.AddRange(army);
       }

       public void AddHeroe(HeroesFactory factory)
       {
          
           heroes.Add(new HumanHeroe(factory));
       }

        public double getArmyStats()
        {
            double stats = 0;
            foreach (var humanUnit in units)
            {
                stats += humanUnit.getStats();
            }
            return stats;
        }
       //public IArmyAction Clone()
       //{
       //   return _action.Clone();
       //    //return MemberwiseClone() as IArmyAction;
       //}

        //public override void BuildCamp()
        //{
        //    Console.WriteLine("Camp builded");
        //}
       public void BaffHumanUnits(HumanHeroe heroe)
       {
           heroe.BaffArmy(units.ToArray());
       }

       public void Baffer(IBuffer baff)
       {
           baff.Buff();
       }
       //public object DeepCopy()
       //{
       //    object army = null;
       //    using (var tempStream = new MemoryStream())
       //    {
       //        var binForm = new BinaryFormatter(null, new StreamingContext(StreamingContextStates.Clone));
       //        binForm.Serialize(tempStream, this);
       //        tempStream.Seek(0, SeekOrigin.Begin);

       //        army = binForm.Deserialize(tempStream);
       //    }
       //    return army;
       //}
      
    }
}
