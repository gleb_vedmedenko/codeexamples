﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patterns_Preconditions
{
  public  class OBuffer:IBuffer
    {
       private readonly OrcArmy _army;
       private readonly OrcHeroe _heroe;
       public OBuffer(OrcArmy army, OrcHeroe heroe)
       {
           _army = army;
           _heroe = heroe;
       }

       public void Buff()
       {
           _army.BuffOrcUnits(_heroe);
       }
    }
}
