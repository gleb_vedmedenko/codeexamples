﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patterns_Preconditions
{
   public class HBuffer:IBuffer
   {
       private readonly HumanArmy _army;
       private readonly HumanHeroe _heroe;
       public HBuffer(HumanArmy army,HumanHeroe heroe)
       {
           _army = army;
           _heroe = heroe;
       }

       public void Buff()
       {
           _army.BaffHumanUnits(_heroe);
       }
    }
}
