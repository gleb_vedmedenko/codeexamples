﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using Patterns_AbstractFactory;

namespace Patterns_Preconditions
{
    [Serializable]
    public class OrcArmy :Army,IOBuffer
    {
       public List<OrcUnit> units { get; private set; }
       public List<OrcHeroe> heroes { get; private set; }
     

       public OrcArmy(OArmyAction action,OArmyCreator creator):base(action,creator)
       {
           units = new List<OrcUnit>();
           heroes = new List<OrcHeroe>();
           creator.army = this;
           action.army = this;
       }

       public void AddOrcUnit(int count, OrcFactory factory)
       {
           var army = new OrcUnit[count];
           for (int i = 0; i < count; i++)
           {
               army[i] = new OrcUnit(factory);
           }
           units.AddRange(army);
       }

       public void AddHeroe(HeroesFactory factory)
       {
           heroes.Add(new OrcHeroe(factory));
       }
       //public IArmyAction Clone()
       //{
       //    return MemberwiseClone() as IArmyAction;
       //}
       public void BuffOrcUnits(OrcHeroe heroe)
       {
           heroe.BaffArmy(units.ToArray());
       }

       public void Buffer(IBuffer buffer)
       {
           buffer.Buff();
       }

       //public object DeepCopy()
       //{
       //    object army = null;
       //    using (var tempStream = new MemoryStream())
       //    {
       //        var binForm = new BinaryFormatter(null, new StreamingContext(StreamingContextStates.Clone));
       //        binForm.Serialize(tempStream, this);
       //        tempStream.Seek(0, SeekOrigin.Begin);

       //        army = binForm.Deserialize(tempStream);
       //    }
       //    return army;
       //}
    }
}
