﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exceptions
{
    public struct EnumExceptions
    {
        public enum TypeException
        {
            InvalidCastException,
            IndexOutOfRangeException,
            NullReferecnceException,
            Success,
            MyOwnException
        }
        
    }
}
