﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Exceptions
{
    public struct Employee
    {
        private Levels levels;
        public Levels.EmployeeLevel level { get; set; }
        public string Name { get; set; }
        public int Stage { get; set; }
        private static  Random rand = new Random();

        public void SetParams()
        {
            setName();
            setLevel();
            setStage();
        }

        public bool DoTaks(Levels.TaskLevel[] tasks,int plan, out int realTime)
        {
            if(tasks == null || tasks.Length < 1)
                throw new ArgumentException();
            switch (level)
            {
                    case Levels.EmployeeLevel.junior:
                    realTime = RandomTime(plan);
                    IncreaseLevel(realTime);
                    if (plan > realTime)
                        return false;
                        return true;
                    case Levels.EmployeeLevel.middle:
                    plan = plan/2;
                    realTime = RandomTime(plan);
                    IncreaseLevel(realTime);
                    if (plan > realTime)
                        return false;
                        return true;
                    case Levels.EmployeeLevel.senior:
                    plan = plan - (int)(plan*0.95);
                    realTime = RandomTime(plan);
                    IncreaseLevel(realTime);
                    if (plan > realTime)
                        return false;
                        return true;
            }
            realTime = 0;
            return false;
        }
        public int CalculatePlan(Levels.TaskLevel[] tasks)
        {
            var plan = 0;
            foreach (var value in tasks)
            {
                plan += (int) value;
            }
            return plan;
        }
        private int RandomTime(int plan)
        {
            return plan*rand.Next(1, 2);
        }
 
        private void setLevel()
        {
            level = (Levels.EmployeeLevel)int.Parse(Console.ReadLine());// Wrong parse
            switch (level)
            {
                case Levels.EmployeeLevel.junior:
                    Stage = 0;
                    break;
                case Levels.EmployeeLevel.middle:
                    Stage = 5000;
                    break;
                case Levels.EmployeeLevel.senior:
                    Stage = 10000;
                    break;
                default:
                    Stage = 0;
                    break;
            }
        }

        private void IncreaseLevel(int exp)
        {
            Stage += exp;
            if(Stage> 5000 && Stage < 10000)
                level = Levels.EmployeeLevel.middle;
            else if(Stage > 10000)
                level = Levels.EmployeeLevel.senior; 
        }
        private void setName()
        {
              var name = Console.ReadLine();
                var regex = new Regex(@"[0-9]|[!@#$%^&*(){}=-?+\/.;,]", RegexOptions.Compiled | RegexOptions.IgnoreCase);
                if (regex.IsMatch(name) || name == null)
                    throw new FormatException();
                Name = name;
        }

        private void setStage()
        {
            Stage += int.Parse(Console.ReadLine());
           
        }


        
    }
}
