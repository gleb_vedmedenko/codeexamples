﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Exceptions
{
   public  static class View
    {
       public static void ShowWorkDone(int plan,int realTime,string name, Levels.EmployeeLevel level,bool isDone )
       {
           if (isDone)
           Console.WriteLine("Сотрудник {0} успел справиться с задачей за {1} минут и вложился в указанное время. Он получил {2} опыта его уровень {3}",
               name,plan,plan,level);
           else
               Console.WriteLine("Сотрудник {0} не успел справиться с задачей за {1} минут и не вложился в указанное время. Он получил {2} опыта его уровень {3}",
                   name, plan,realTime, level);
       }

       public static void ShowExceptions(Exception e, EnumExceptions.TypeException type)
       {
           if(type == EnumExceptions.TypeException.Success)
               Console.WriteLine("Success");
           else
           Console.WriteLine(e.Message);
          
       }
    }
}
