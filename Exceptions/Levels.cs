﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exceptions
{
    public struct Levels
    {
        public enum TaskLevel
        {
            weak =30,
            easy =60,
            norman =90,
            hard = 450,
            impossible = 1200
        }
        public enum EmployeeLevel
        {
            junior,middle,senior
        }
    }
}
