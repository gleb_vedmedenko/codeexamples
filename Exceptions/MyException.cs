﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Mime;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Exceptions
{
   public class MyException
   {
       public EnumExceptions.TypeException exceptions { get; set; }
       const string name = "application.log";
       private string path = string.Format("{0}{1}", AppDomain.CurrentDomain.BaseDirectory,name);
 
       public void getException(EnumExceptions.TypeException type)
       {
           double[] array = new double[5];
           switch (type)
           {
               case  EnumExceptions.TypeException.IndexOutOfRangeException:
                   try
                   {
                       double a = array[6];
                   }
                   catch (IndexOutOfRangeException ex)
                   {
                       StackTrace stackTrace = new StackTrace();
                       StackFrame[] methodBase = stackTrace.GetFrames();
                       MethodBase[] bases = new MethodBase[methodBase.Length];
                       for (int i = 0; i < methodBase.Length; i++)
                       {
                           bases[i] = methodBase[i].GetMethod();
                       }
                       StringBuilder sb = new StringBuilder();
                       for (int i = 0; i < bases.Length; i++)
                       {
                           sb.Append(bases[i].Name);
                           sb.Append(" ");
                       }
                       Log(string.Format("Exception : {0} Mehthods : {1} ", ex.Message, sb));
                    
                   }
                   break;
               case EnumExceptions.TypeException.InvalidCastException:
                   try
                   {
                       StringBuilder bd = new StringBuilder();
                       object refer = bd;
                       StreamReader read = (StreamReader) refer;
                   }
                   catch (InvalidCastException ex)
                   {
                       StackTrace stackTrace = new StackTrace();

                       StackFrame[] methodBase = stackTrace.GetFrames();
                       MethodBase[] bases = new MethodBase[methodBase.Length];
                       for (int i = 0; i < methodBase.Length; i++)
                       {
                           bases[i] = methodBase[i].GetMethod();
                       }
                       StringBuilder sb = new StringBuilder();
                       for (int i = 0; i < bases.Length; i++)
                       {
                           sb.Append(bases[i].Name);
                           sb.Append(" ");
                       }
                       Log(string.Format("Exception : {0} Mehthods : {1} ", ex.Message, sb));
                   }
                 
                  break;
               case EnumExceptions.TypeException.NullReferecnceException:
                   try
                   {
                       double s = array[1];
                   }
                   catch (NullReferenceException ex)
                   {
                       StackTrace stackTrace = new StackTrace();
                       StackFrame[] methodBase = stackTrace.GetFrames();
                       MethodBase[] bases = new MethodBase[methodBase.Length];
                       for (int i = 0; i < methodBase.Length; i++)
                       {
                           bases[i] = methodBase[i].GetMethod();
                       }
                       StringBuilder sb = new StringBuilder();

                       for (int i = 0; i < bases.Length; i++)
                       {
                           sb.Append(bases[i].Name);
                           sb.Append(" ");
                       }
                       Log(string.Format("Exception : {0} Mehthods : {1} ", ex.Message, sb));
                   }
                  break;
               case EnumExceptions.TypeException.Success:
                   try
                   {
                       throw new Exception("Success");
                   }
                   catch (Exception ex)
                   {
                       StackTrace stackTrace = new StackTrace();
                       StackFrame[] methodBase = stackTrace.GetFrames();
                       MethodBase[] bases = new MethodBase[methodBase.Length];
                       for (int i = 0; i < methodBase.Length; i++)
                       {
                           bases[i] = methodBase[i].GetMethod();
                       }
                       StringBuilder sb = new StringBuilder();
                       for (int i = 0; i < bases.Length; i++)
                       {
                           sb.Append(bases[i].Name);
                           sb.Append(" ");
                       }
                       Log(string.Format("Exception : {0} Mehthods : {1} ", ex.Message, sb));
                   }
                   break;
               case EnumExceptions.TypeException.MyOwnException:
                   try
                   {
                       throw  new MyOwnException("Own Exception");
                   }
                   catch (MyOwnException ex)
                   {
                       StackTrace stackTrace = new StackTrace();
                       StackFrame[] methodBase = stackTrace.GetFrames();
                       MethodBase[] bases = new MethodBase[methodBase.Length];
                       for (int i = 0; i < methodBase.Length; i++)
                       {
                           bases[i] = methodBase[i].GetMethod();
                       }
                       StringBuilder sb = new StringBuilder();
                       for (int i = 0; i < bases.Length; i++)
                       {
                           sb.Append(bases[i].Name);
                           sb.Append(" ");
                       }
                       Log(string.Format("Exception : {0} Mehthods : {1} ", ex.Message, sb));
                   }
                   break;
                   
           }

       }

       public void getException(EnumExceptions.TypeException[] type)
       {
           for (int i = 0; i < type.Length; i++)
           {
               getException(type[i]);
           }
       }

       public void Log(string mess)
       {
           if (File.Exists(path))// check here
           {
               using (StreamWriter sw = File.AppendText(path))
               {
                   StringBuilder build = new StringBuilder();
                   build.Append(mess).Append("\n");
                    sw.WriteLine(build);
               }	 
           }
           else
           {
               using (StreamWriter wr = File.AppendText(path))
               {
                   StringBuilder build = new StringBuilder();
                   build.Append(mess).Append("\n");
                   wr.WriteLine(build);
               }
           }

       }

   }
}
