﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exceptions
{
    class Program
    {
        private static Random _rand = new Random();

        static void Main(string[] args)
        {

            Employee empl = new Employee();
            empl.SetParams();
            empl.Display();
            Levels.TaskLevel[] levels = { Levels.TaskLevel.easy, Levels.TaskLevel.weak };
            empl.setLevel();
            Levels.TaskLevel[] tasks = { Levels.TaskLevel.easy, Levels.TaskLevel.weak };
            var plan = empl.CalculatePlan(tasks);
            var realTime;
            bool isDone = empl.DoTaks(tasks, plan, out realTime);
            Console.WriteLine(empl.Stage);
            View.ShowWorkDone(plan, realTime, empl.Name, empl.level, isDone);
            Console.WriteLine(empl.Stage);
            Console.WriteLine(empl.level);
            MyException ex = new MyException();
            var exec = RandomEnums(5);
            ex.getException(exec);
           
        }

        public static EnumExceptions.TypeException[] RandomEnums(int count)
        {
            var excepd = new EnumExceptions.TypeException[count]; 
            for (int i = 0; i < count; i++)
            {
                excepd[i] = (EnumExceptions.TypeException)_rand.Next(0, 5);
            }
            return excepd;
        }

    }
}
