﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using Assets.Script.Model;
using Assets.Script.View;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Script.ModelView
{
    public class UserSignContext:Context
    {
        public string Mail
        {
            get
            {
                return mail;
            }
            set
            {
                mail = value;
                
                //  DataChanged("HOLA");
            }
        }

        public string Password { get; set; }
        private string mail;
        public RequestType Request {get{return RequestType.SignIn;}}
        public event Change fieldChange; 
        public delegate void Change(InputField field,string s);
        public  void  setChange(InputField field,string s)
        {
            fieldChange(field,s);
           
        }

        public void some(InputField field,string s)
        {
            field.text = s;
        }
    }
}
