﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Assets.Script.Model;
using Assets.Script.ModelView;
using Assets.Script.View;
using UnityEngine;

namespace Assets.Script
{
   public static class AppViewModel
   {

       public static UserLoginContext logInContext;
       public static UserSignContext signInContext;
       public static ResourceContext resourceContext;   
       public static Action<RequestType> Send;
       public static Dictionary<string, Context> contexts;
        
       static AppViewModel()
       {

         logInContext = new UserLoginContext();
         signInContext = new UserSignContext();
         resourceContext = new ResourceContext();
         contexts = new Dictionary<string, Context>();
         contexts.Add("LogIn", logInContext);
         contexts.Add("SignIn", signInContext);
         contexts.Add("Update", resourceContext);
         AppModel.UserChanged += TranslateUser;
         AppModel.ResourceChanged += TranslateResources;
         Send += AppModel.Execute;

       }
      
       public static void TranslateUser(User user)
       {
           if (user == null)
               AppView.message = AppModel.message;
           AppView.data = user;

       }

       public static void TranslateResources(UserData data)
       {
           if (data == null)
               AppView.message = AppModel.message;
           AppView.data.userData = data;
      
       }
       public static void SendToServer(RequestType type)
       {
           AppModel.Execute(type);
       }

       public static void SendMessage(string message)
       {
           AppView.SystemMessage(message);
       }

     
      
    }
}
