﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using Assets.Script.Model;

namespace Assets.Script.ModelView
{
   public class UserLoginContext:Context
   {


       public RequestType Request { get { return RequestType.Register; } } 
       public string Name { get; set; }
       public string Mail { get; set; }
       public string Password { get; set; }

   }
}
