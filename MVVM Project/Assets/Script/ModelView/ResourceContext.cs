﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.Script.Model;
using UnityEngine.UI;

namespace Assets.Script.ModelView
{
    public class ResourceContext : Context
    {
        public RequestType Request { get { return RequestType.GetData; } }

        public string Money
        {
            get
            {
                return _money;

            }
            set
            {
                _money = value;
                MoneyChanged(_money);
            }
        }

        public string Food
        {
            get
            {
                return _food;
            }
            set
            {

                _food = value;
                FoodChanged(_food);
            }
        }
        public string Material
        {
            get
            {
                return _material;
            }
            set
            {
                _material = value;
                MaterialChanged(_material);
            }
        }
       
        
        public string BaseLvl
        {
            get
            {
                return _baseLvl;
            }
            set
            {
                _baseLvl = value;
                BaseChanged(_baseLvl);
            }
        }

        public string speedMining
        {
            get
            {
                return _miningSpeed;
            }
            set
            {
                _miningSpeed = value;
                MiningSpeedChanged(_miningSpeed);
            }
        }
        public string ID { get; set; }

        public static event Change MoneyChanged;
        public static event Change FoodChanged;
        public static event Change MaterialChanged;
        public static event Change BaseChanged;
        public static event Change MiningSpeedChanged;

        public delegate void Change(string s);

        private string _money;
        private string _material;
        private string _food;
        private string _baseLvl;
        private string _miningSpeed;


    }
}
