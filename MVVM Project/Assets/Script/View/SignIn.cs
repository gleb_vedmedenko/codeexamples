﻿using System;
using Assets.Script;
using Assets.Script.Model;
using Assets.Script.ModelView;
using Assets.Script.View;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Assets.Script.View
{
    public class SignIn : MonoBehaviour, IPointerClickHandler
    {

        public InputField Mail;
        public InputField Password;
        public UpgradeUser UserData;
        public Canvas signIn;
        public BindWriter bindWriter;

      
        public void OnPointerClick(PointerEventData eventData)
        {


            if (Mail != null && Password != null)
            {

                // AppView.Transport(Mail.text, Password.text, RequestType.SignIn);
                // AppView.setViewData(AppView.signInContext.userContext);
            
                AppView.TranslateUser(bindWriter.type);
             //   bindWriter.read(AppViewModel.contexts[bindWriter.Context]);
                var message = AppView.data;              
                ShowMessage(message);
            }
            else
            {
                throw new NullReferenceException();
            }

        }
       
        private void ShowMessage(User message)
        {
            if (message != null)
            {
              
                signIn.enabled = false;
                UserData.userDataPanel.enabled = true;
                UserData.food.text = message.userData.Food.ToString();
                UserData.money.text = message.userData.Money.ToString();
                UserData.materials.text = message.userData.Materials.ToString();
                UserData.baseLvl.text = message.userData.BuildingLevel.ToString();
                UserData.userName.text = message.Name;
               
            }
            else
            {
                Debug.Log(AppView.message);
            }
        }
    }
}