﻿
using UnityEngine;
using UnityEngine.EventSystems;

public class ShowPanel : MonoBehaviour,IPointerClickHandler
{
    public Canvas signInPanel;
    public Canvas registerPanel;
    public Canvas userDataPanel;
    public void Start()
    {
        userDataPanel.enabled = false;
        registerPanel.enabled = false;
        signInPanel.enabled = !registerPanel.enabled;
    }
    public void OnPointerClick(PointerEventData eventData)
    {
        if (!userDataPanel.enabled)
        {
            if (!registerPanel.enabled)
            {
                registerPanel.enabled = true;
                signInPanel.enabled = false;
            }
            else
            {
                registerPanel.enabled = false;
                signInPanel.enabled = true;
            }
        }
        else
        {
            registerPanel.enabled = false;
            signInPanel.enabled = false;

        }
        
    }
}
