﻿using System;
using Assets.Script;
using Assets.Script.Model;
using UnityEngine.UI;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Assets.Script.View
{
    public class Register : MonoBehaviour, IPointerClickHandler
    {

        public Text Name;
        public Text Mail;
        public Text Password;
        public Canvas UserDataPanel;
        public Canvas RegisterPanel;
        public UpgradeUser UserData;
        public BindWriter BindWriter;
        
        public void OnPointerClick(PointerEventData eventData)
        {
            if (Name != null && Mail != null && Password != null)
            {
                AppView.TranslateUser(BindWriter.type);
                var message = AppView.data;
                ShowMessage(message);
            }
            else
            {
                throw new NullReferenceException();
            }
        }

        private void ShowMessage(User message)
        {
            if (message != null)
            {
                UserDataPanel.enabled = true;
                RegisterPanel.enabled = false;
                UserData.userDataPanel.enabled = true;
                UserData.food.text = message.userData.Food.ToString();
                UserData.money.text = message.userData.Money.ToString();
                UserData.materials.text = message.userData.Materials.ToString();
                UserData.baseLvl.text = message.userData.BuildingLevel.ToString();
                UserData.userName.text = message.Name;
                Debug.Log("Welcome to the game");
            }
            else
            {
                Debug.Log(AppView.message);
            }
        }
    }
}