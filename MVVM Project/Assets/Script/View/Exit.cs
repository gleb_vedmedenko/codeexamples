﻿using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine;

public class Exit : MonoBehaviour,IPointerClickHandler {
    public Canvas signInPanel;
    public Canvas registerPanel;
    public Canvas userDataPanel;

    public void OnPointerClick(PointerEventData data)
    {
        userDataPanel.enabled = false;
        signInPanel.enabled = true;
        
    }
	
}
