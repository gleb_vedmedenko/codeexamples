﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Xml;
using Assets.Script;
using Assets.Script.Model;
using Assets.Script.ModelView;
using Assets.Script.View;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Script.View
{
    public class BindWriter : MonoBehaviour
    {
        public InputField field;
        public string Property;
        public string Context;
        [HideInInspector]
        public RequestType type;
        public bool isWrite;
        private string eventName;
        // Use this for initialization
        private void Start()
        {
            eventName = string.Format("{0}{1}", Property, "Change");
            var bind = new Binding(Property, AppViewModel.contexts[Context]);
            field.onEndEdit.AddListener(delegate { bind.sendToContext(field, out type); });
            Debug.Log(eventName);
            if (isWrite)
                read(bind.Context);

        }
        public void Read(object context)
        {
            var type = context.GetType();
            var eventInfo = type.GetEvent(eventName);
            var writer = this;
            var handler = Delegate.CreateDelegate(eventInfo.EventHandlerType, writer, GetType().GetMethod("propChange"));
            Debug.Log(handler);
            eventInfo.AddEventHandler(context, handler);

        }

        public void PropChange(string s)
        {

            field.text = s;
        }

    }
}