﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Xml.Schema;
using Assets.Script.Model;
using Assets.Script.ModelView;
using UnityEditor;
using UnityEngine.UI;
using UnityEngine;
namespace Assets.Script.View
{
   public class Binding
    {
       public string Text { get; private set; }
       public object Context { get; private set; }
     

       public Binding(string Property,Context context)
       {

           Context = context;
           Text = Property;
       }

       public void sendToContext(InputField Data, out RequestType request)
       {          
           var message = Data.text;
           var type = Context.GetType();
           type.GetProperty(Text).SetValue(Context, message, null);
           var s = type.GetProperty("Request").GetValue(Context, null);
           request = (RequestType)s;
       }

       public string getFromContext( out RequestType request)
       {        
           var type = Context.GetType();
           var s = type.GetProperty("Request").GetValue(Context, null);
           request = (RequestType)s;
           var str = type.GetProperty(Text).GetValue(Context, null);
           return str.ToString();
       }

       
    }
}
