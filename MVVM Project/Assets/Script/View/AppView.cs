﻿using System;
using Assets.Script.Model;
using Assets.Script.ModelView;
using Assets.Script.View;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Script
{
    static class AppView
    {
        public static string message;
        public static User data;
        public static UserData userData;      
        public static Action<string> idEvent;
     
        public static void TranslateUser(RequestType type)
        {
            AppViewModel.Send(type);
        }
        public static void SystemMessage(string viewMessage)
        {
            message = viewMessage;
        }
       
    }
}
