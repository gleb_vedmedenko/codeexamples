﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.Script.Model;
using UnityEngine;

namespace Assets.Script.View
{
    public class BindReader:MonoBehaviour
    {
        public string Property;
        public string Context;
        [HideInInspector]
        public RequestType type;
        [HideInInspector]
        public string Value;
        // Use this for initialization
        private void Start()
        {
            var bind = new Binding(Property, AppViewModel.contexts[Context]);
            Value = bind.getFromContext(out type);
        }
    }
}
