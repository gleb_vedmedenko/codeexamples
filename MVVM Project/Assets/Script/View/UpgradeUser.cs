﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net.Mime;
using Assets.Script;
using Assets.Script.Model;
using Assets.Script.View;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class UpgradeUser : MonoBehaviour, IPointerClickHandler
{

    public InputField food;
    public InputField money;
    public InputField materials;
    public Text baseLvl;
    public Text userName;
    public Canvas userDataPanel;
    public BindReader BindReader;
    public string userUID;
    public double speedMining;
    private float tempTimer = 1f;
    const float timer = 1f;
   
    public void OnPointerClick(PointerEventData eventData)
    {

        AppView.TranslateUser(RequestType.GetData);
        
    
    }

   public void Start()
    {
        
    }
   public void Update()
    {
      
     if(userDataPanel.enabled)
     Timer();   // better use coroutine 
    }

    void Timer()
    {
        tempTimer -= Time.deltaTime;
        if (tempTimer <= 0)
        {
            
            food.text = string.Format("{0}",double.Parse(food.text) + speedMining);
            materials.text = string.Format("{0}", double.Parse(materials.text) + speedMining);
            money.text = string.Format("{0}", double.Parse(money.text) + speedMining);
            tempTimer = timer;
        }
    }

    private void ShowMessage(User message)
    {
        if (message != null && AppView.message == null)
        {

            food.text = message.userData.Food.ToString();
            materials.text = message.userData.Materials.ToString();
            money.text = message.userData.Money.ToString();
            baseLvl.text = message.userData.BuildingLevel.ToString();
            speedMining = message.userData.miningSpeed;
            userName.text = message.Name;
        }
        else
        {
            Debug.Log(AppView.message);
        }
    }

   
}
