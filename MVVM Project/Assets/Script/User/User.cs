﻿using System;
using Assets.Script.Model;
using UnityEngine;
namespace Assets.Script
{
    [Serializable]
    public class User
    {

      
        public string Name;        
        public string Mail;
        public string Password;
        public string ID;
        public RequestType request;       
        public UserData userData;

        public User(string name, string mail,string password,RequestType type)
        {
            Name = name;
            Password = password;
            Mail = mail;
            request = type;         
        }

        
      
    }
   

}
