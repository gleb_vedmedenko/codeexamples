﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Script.Model
{
    public enum RequestType
    {
        Register = 0,
        SignIn= 1,
        GetData = 2
    }
}
