﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Asset.Script;
using UnityEngine;

namespace Assets.Script.Model
{
   public class UpdateBaseCommand:Command
    {
       public override void Execute(UserData data, RequestType type)
       {
           string json;
           var user  = new User("","","",type);
           user.userData = data;
           Client.Proc(user, out json);
           Desirialize(json);
       }

       public override void Execute(string UID, RequestType type)
       {
           string json;
           var user = new User("", "", "", type);
           user.ID = UID;
           Client.Proc(user, out json);
           Desirialize(json);
       }

       public override void Desirialize(string json)
       {
           
           switch (json)
           {
               case "0":
                   AppViewModel.SendMessage("Wrong Data");
                   break;
               case "1":
                   AppViewModel.SendMessage("Account already exist");
                   break;
               case "2":
                   AppViewModel.SendMessage("Not Enought Resources");
                   break;
               default:
                    AppModel.userDataProp = JsonUtility.FromJson<UserData>(json);
                    AppViewModel.resourceContext.Money = AppModel.userDataProp.Money.ToString();
                    AppViewModel.resourceContext.Food = AppModel.userDataProp.Food.ToString();
                    AppViewModel.resourceContext.Material = "100";//AppModel.userDataProp.Materials.ToString();
                   // AppViewModel.resourceContext.BaseLvl = AppModel.userDataProp.BuildingLevel.ToString();
                   break;
           }
       }
    }
}
