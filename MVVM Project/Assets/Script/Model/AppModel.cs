﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using Asset.Script;
using Assets.Script.Model;
using Assets.Script.ModelView;
using UnityEditor.Purchasing;
using UnityEngine;
using UnityEngine.VR.WSA.Persistence;

namespace Assets.Script
{
   public static class AppModel
   {
       public static Dictionary<RequestType, Context> command = new Dictionary<RequestType, Context>();

       public static User userProp
       {
           get
           {
               return User;
           }
           set
           {
               User = value;
               UserChanged(User);

           }
       }

       public static UserData userDataProp
       {
           get
           {
               return User.userData;
               
           }
           set
           {            
                User.userData = value;
                ResourceChanged(User.userData);           
           }
       }
       public static User User;    
       public static LogInCommand logIn;
       public static SignInCommand signIn;
       public static UpdateBaseCommand updateBase;
       public static string message;
       static AppModel()
       {
           logIn = new LogInCommand();
           signIn = new SignInCommand();
           updateBase = new UpdateBaseCommand();
           
       }
        public static Action<User> LogInEvent;
        public static Action<User> UserChanged;
        public static Action<UserData>  ResourceChanged;

      
       public static void Execute(RequestType type)
       {

           switch (type)
           {
               case RequestType.SignIn:
                   signIn.Execute(Validate.ParseMail(AppViewModel.signInContext.Mail), Validate.ParsePassword(AppViewModel.signInContext.Password), type);
                   break;
               case RequestType.Register:
                   logIn.Execute(Validate.ParseMail(AppViewModel.logInContext.Mail),Validate.ParsePassword(AppViewModel.logInContext.Password), Validate.ParseName(AppViewModel.logInContext.Name), type);
                   break;
               case RequestType.GetData:       
                   updateBase.Execute(AppModel.userProp.ID, type);
                   break;
           }
       }
    }
}
