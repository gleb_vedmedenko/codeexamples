﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Asset.Script;
using UnityEditor;
using UnityEngine;

namespace Assets.Script.Model
{
   public class SignInCommand:Command
    {
       public override void Execute(string Mail, string Password, RequestType type)
       {
           var user = new User("", Mail, Password, type);
           string json;
           Client.Proc(user, out json);
           Desirialize(json);
       }
       public override void Desirialize(string json)
       {
           switch (json)
           {
               case "0":
                   AppViewModel.SendMessage("Wrong Data");
                   break;
               case "1":
                   AppViewModel.SendMessage("Account already exist");
                   break;
               case "2":
                   AppViewModel.SendMessage("Not Enought Resources");
                   break;
               default:
                   AppModel.userProp = JsonUtility.FromJson<User>(json);             
                   break;
           }
       }
    }
}
