﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Assets.Script.Model
{
  public static  class Validate
    {
        public static string ParseName(string name)
        {
            if (name[0] == ' ')
                throw new FormatException();
            var pattern = @"^[a-zA-Z0-9\_]+$";
            if (Regex.Match(name, pattern).Success)
                return name;
            throw new FormatException();
        }


        public static string ParseMail(string mail)
        {
            var pattern = "^([0-9a-zA-Z]([-\\.\\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\\w]*[0-9a-zA-Z]\\.)+[a-zA-Z]{2,9})$";
            if (Regex.IsMatch(mail, pattern))
                return mail;
            throw new FormatException();
        }

        public static string ParsePassword(string password)
        {
            var pattern = @"^[a-zA-Z0-9]+$";
            if (Regex.Match(password, pattern).Success)
                return password;
            throw new FormatException();
        }
    }
}
