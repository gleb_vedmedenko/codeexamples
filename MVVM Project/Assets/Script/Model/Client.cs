﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using Assets.Script;
using UnityEngine;


namespace Asset.Script
{
    internal class Client
    {

        public static void Proc(User user, out string message)
        {
            var request = (HttpWebRequest) HttpWebRequest.Create("http://localhost:8888/connection");
            var jsonUser = JsonUtility.ToJson(user);
            var data = Encoding.UTF8.GetBytes(jsonUser);
            request.Method = "POST";
            request.ContentLength = data.Length;
            using (var stream = request.GetRequestStream())
            {
                stream.Write(data, 0, data.Length);
            }
            var resp = (HttpWebResponse) request.GetResponse();
            using (StreamReader stream = new StreamReader(resp.GetResponseStream(), Encoding.UTF8))
            {
                    message = stream.ReadToEnd();
            }
        }
        //public static void Proc(UserData user, out string message)
        //{
        //    var request = (HttpWebRequest)HttpWebRequest.Create("http://localhost:8888/connection");
        //    var jsonUser = JsonUtility.ToJson(user);
        //    var User = Encoding.UTF8.GetBytes(jsonUser);
        //    request.Method = "POST";
        //    request.ContentLength = User.Length;
        //    using (var stream = request.GetRequestStream())
        //    {
        //        stream.Write(User, 0, User.Length);
        //    }
        //    var resp = (HttpWebResponse)request.GetResponse();
        //    using (StreamReader stream = new StreamReader(resp.GetResponseStream(), Encoding.UTF8))
        //    {

        //        message = stream.ReadToEnd();

        //    }
        //}
    }
}
