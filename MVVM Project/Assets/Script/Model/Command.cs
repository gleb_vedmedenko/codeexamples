﻿using UnityEngine;

namespace Assets.Script.Model
{
   public  abstract class Command
   {
       public virtual void Execute(string name, string mail, string password, RequestType type)
       {
           
       }

       public virtual void Execute(string mail, string password, RequestType type)
       {

       }

       public virtual void Execute(UserData user, RequestType type)
       {
           
       }

       public virtual void Execute(string UID, RequestType type)
       {
           
       }

       public abstract void Desirialize(string json);

   }


}
