﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HardLINQ
{
    public class CustomUnit:Unit
    {
        
        public CustomUnit(string name,int attackPower,int defenseInfantry,
            int defenseArcher,int defenseCavalry,int healthPoint,bool range,
            bool defense
            ):base(name)
        {
            CurrentHealth = healthPoint;
            AttackPower = attackPower;
            DefenseInfantry = defenseInfantry;
            DefenseArcher = defenseArcher;
            DefenseCavalry = defenseCavalry;
            Range = range;
            Defense = defense;
        }
        public override void Attack(ref Unit unit)
        {
            unit.CurrentHealth -= AttackPower;
        }
    }
}
