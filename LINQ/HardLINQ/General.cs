﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace HardLINQ
{
   public class General
   {
       public List<Unit> units = new List<Unit>();

       public General(int archersCount, int cavalryCount, int infantryCount)
       {
           for (int i = 0; i < archersCount; i++)
           {
               AddUnits(new Archer(new Point(0, 0), "Archer"));
           }          
           for (int i = 0; i < cavalryCount; i++)
           {
               AddUnits(new Cavalry(new Point(0, 0), "Cavalry"));  
           }
           for (int i = 0; i < infantryCount; i++)
           {
               AddUnits(new Infantry(new Point(0, 0), "Infantry"));
           }
          
       }

       public void Recruit(Unit type1, Unit type2)
       {

           units.Where(un => un.GetType() == type2.GetType())
               .Select(un =>
               {
                   un.UnitName = "NEXT";
                   un.AttackPower = type1.AttackPower + type2.AttackPower;
                   un.CurrentHealth = 100;
                   return un;
               }).ToList();
       }

       public Unit Train(Unit inputUnit, Unit outputUnit)
       {
           Unit[] typeInput = units.Where(t => t.GetType() == inputUnit.GetType()).ToArray();
           Unit[] typeResult = units.Where(t => t.GetType() == outputUnit.GetType()).ToArray();
           
           var newUnits = typeInput
               .Join(
                   typeResult,
                   e => e.UID,
                   o => o.UID,
                   (e, o) => new
                   {
                       name = string.Format("{0} {1}",e.UnitName,o.UnitName),
                       attackPower = e.AttackPower > o.AttackPower ? e.AttackPower : o.AttackPower,
                       defenseInfantry = e.DefenseInfantry > o.DefenseInfantry ? e.DefenseInfantry : o.DefenseInfantry,
                       defenseArcher = e.DefenseArcher > o.DefenseArcher ? e.DefenseArcher : o.DefenseArcher,
                       defenseCavalry = e.DefenseCavalry > o.DefenseCavalry ? e.DefenseCavalry : o.DefenseCavalry,
                       healthPoints = e.CurrentHealth > o.CurrentHealth ? e.CurrentHealth : o.CurrentHealth,
                       range = o.Range ? true : e.Range,
                       defense = o.Defense ? true : e.Defense
                   }
               ).ToList();
           var newUnit = newUnits[0];
      
         Unit unit = new CustomUnit(newUnit.name,newUnit.attackPower,newUnit.defenseInfantry,newUnit.defenseArcher,
             newUnit.defenseCavalry,newUnit.healthPoints,newUnit.range,newUnit.defense);
         return unit;
       }
   
       public Unit[] Inspire()
       {
           int count = Randomizer.getRandom(units.Count);
           var inspireUnits = units.Take(count).ToArray();
           inspireUnits.Select(unit =>
           {
               unit.CurrentHealth += 2;
               return unit;
           }).ToArray();
           
           return inspireUnits;
       }

       private void AddUnits( Unit type)
       {
           Unit unit = type;
           units.Add(unit);
       }

   }
}
