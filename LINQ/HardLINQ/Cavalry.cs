﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HardLINQ
{
    public class Cavalry:Unit
    {
        public readonly int attackPower = 12;
        public readonly int defenseInfantry = 10;
        public readonly int defenseArcher = 5;
        public readonly int defenseCavalry = 10;
        public readonly string Name = "Cavalry";
        public readonly int healthPoint = 20;


        public Cavalry(Point p, string Name)
            : base(Name)
        {
            point = p;
            CurrentHealth = healthPoint;
            Defense = false;
            Range = false;
            setParams();
        }
        public override void Attack(ref Unit unit)
        {
            unit.CurrentHealth -= attackPower;
        }
        private void setParams()
        {
            CurrentHealth = healthPoint;
            AttackPower = attackPower;
            DefenseInfantry = defenseInfantry;
            DefenseArcher = defenseArcher;
            DefenseCavalry = defenseCavalry;
        }
    }
}
