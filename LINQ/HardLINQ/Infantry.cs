﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HardLINQ
{
    public class Infantry:Unit
    {
        public  int attackPower = 10;
        public  int defenseInfantry = 10;
        public  int defenseArcher = 8;
        public  int defenseCavalry = 12;
        public  string Name = "Cavalry";
        public  int healthPoint = 15;


        public Infantry(Point p, string Name)
            : base(Name)
        {
            point = p;
            CurrentHealth = healthPoint;
            Defense = true;
            Range = false;
            setParams();
        }
        public override void Attack(ref Unit unit)
        {
            unit.CurrentHealth -= attackPower;
        }
        private void setParams()
        {
            CurrentHealth = healthPoint;
            AttackPower = attackPower;
            DefenseInfantry = defenseInfantry;
            DefenseArcher = defenseArcher;
            DefenseCavalry = defenseCavalry;
        }
    }
}
