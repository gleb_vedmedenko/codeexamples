﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HardLINQ
{
  public abstract  class Unit
    {
      
        public Point point { get; set; }
        public int CurrentHealth { get; set; }
        public bool Defense { get; set; }
        public bool Range { get; set; }
        public int AttackPower { get; set; }
        public int DefenseInfantry { get; set; }
        public int DefenseArcher { get; set; }
        public int DefenseCavalry { get; set; }        
        public string UnitName { get; set; }
        public abstract void Attack(ref Unit unit);
        public readonly int UID = 1;

      protected Unit(string name)
      {
          UnitName = name;
      }
    }
}
