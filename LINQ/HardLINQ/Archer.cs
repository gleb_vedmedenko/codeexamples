﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HardLINQ
{
    public class Archer:Unit
    {
        public  readonly int attackPower = 8;
        public  readonly int defenseInfantry = 5;
        public  readonly int defenseArcher = 5;
        public  readonly int defenseCavalry = 1;
        public readonly int healthPoint = 10;
        public readonly int range = 5;       
        public Archer(Point p,string Name):base(Name)
        {
            point = p;
            Defense = false;
            Range = true;
            setParams();
        }
        public override void Attack(ref Unit unit)
        {
            unit.CurrentHealth -= attackPower;
        }

        private void setParams()
        {
            CurrentHealth = healthPoint;
            AttackPower = attackPower;
            DefenseInfantry = defenseInfantry;
            DefenseArcher = defenseArcher;
            DefenseCavalry = defenseCavalry;
        }
    } 
}
