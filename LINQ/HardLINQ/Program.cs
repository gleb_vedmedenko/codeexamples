﻿using System;
using System.Collections.Generic;

namespace HardLINQ
{
    class Program
    {
        static void Main(string[] args)
        {
            General gen = new General(1,5,2);
            var newUnit = gen.units.Find(x => x.UnitName == "NEXT");
            var unit = gen.Train(gen.units[0], gen.units[3]);
            Console.WriteLine(unit.UnitName + " " + unit.Range + " " + unit.AttackPower);
            Unit[] un = gen.Inspire();
            for (int i = 0; i < un.Length; i++)
            {
                Console.WriteLine(un[i].UnitName + " " + un[i].CurrentHealth);
            }
            Unit cav = new Cavalry(new Point(1, 2), "cav");
            Unit cav2 = new Cavalry(new Point(1, 2), "cav");
            Unit arch = new Archer(new Point(1, 2), "arch");
            List<Unit> un = new List<Unit>();
            un.Add(new Cavalry(new Point(1, 2), "cav"));
            un.Add(new Cavalry(new Point(1, 2), "cav"));

        }
    }
}
