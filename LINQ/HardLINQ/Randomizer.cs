﻿
using System;
using System.Runtime.ConstrainedExecution;

namespace HardLINQ
{
   public static class Randomizer
   {
       private static System.Random _rand;

        static  Randomizer()
       {
           _rand = new System.Random();
       }

       public static int getRandom(int count)
       {
           double minStep = count * 0.1;
           if (minStep < 1) minStep = 1;
           return _rand.Next((int)minStep, count);
       }
   }

}
