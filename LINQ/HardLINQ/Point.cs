﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HardLINQ
{
    public struct Point
    {
        public readonly int X;
        public readonly int Y;

        public Point(int x,int y) : this()
        {
            X = x;
            Y = y;
        }
    }


}
