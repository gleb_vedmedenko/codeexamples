﻿
using System;
using ProjectServer;

namespace Assets.Script
{
    [Serializable]
    public class User
    {
        public string Name;
        public string Mail;
        public string Password;
        public int Request;
        public string ID;
        public UserData userData;
    }
}
