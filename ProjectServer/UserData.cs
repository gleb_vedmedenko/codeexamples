﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Script
{
    [Serializable]
    public class UserData
    {
        public string UID;
        public double Version = 0.0001;
        public double Money;
        public double Food;
        public double Materials;
        public int BuildingLevel = 1;

        public string LastUpdateDate;
        private TimeSpan _timer;
        public double miningSpeed = 0.14;
        public int levelCoast = 500;
        public UserData(double money, double food, double materials, int baseLevel, string ID, DateTime date)
        {
            Money = money;
            Food = food;
            Materials = materials;
            BuildingLevel = baseLevel;
            UID = ID;
            LastUpdateDate = date.ToString();
        }

       
        public UserData()
        {}
        public void levelUp()
        {

            if (hasResource())
            {
                if (BuildingLevel < 25)
                {
                    SpendResource();
                    BuildingLevel++;
                    IncreaseResourceMining();
                    IncreaseLevelCoast();
                    IncreaseVersion();
                    setDate(DateTime.Now);
                }
            }
          
        }

        public void UpdateResource()
        {
            Timer();
        }

        public void SpendResource()
        {
            Food -= levelCoast;
            Materials -= levelCoast;
            Money -= levelCoast;
        }
        public void Timer()
        {
            DateTime LastTime = DateTime.Parse(LastUpdateDate);
            Console.WriteLine(LastUpdateDate);
            var elapsedTicks = DateTime.Now.Ticks - LastTime.Ticks;
            _timer = new TimeSpan(elapsedTicks);
            double plusSeconds  = _timer.TotalSeconds;
            Food += miningSpeed * plusSeconds;
            Materials += miningSpeed * plusSeconds;
            Money += miningSpeed * plusSeconds;
            setDate(DateTime.Now);
        }

        public void IncreaseResourceMining()
        {
            miningSpeed += 0.14;
        }

        public void IncreaseLevelCoast()
        {
            levelCoast += 1;
        }

        public void IncreaseVersion()
        {
            Version += 0.0001;
        }
        public bool hasResource()
        {
            if (Food >= levelCoast && Materials >= levelCoast 
                && Money >= levelCoast)
                return true;
            return false;
        }

        public void setDate(DateTime date)
        {
            LastUpdateDate = date.ToString();
        }

    }
}
