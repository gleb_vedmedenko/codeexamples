﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Assets.Script;

namespace ProjectServer
{
    class Program
    {
        static readonly HttpListener listener = new HttpListener();
        
        static void Main(string[] args)
        {
          
            listener.Prefixes.Add("http://localhost:8888/connection/");
            listener.Start();
            var obj = new ClientObj(listener);        
            obj.Desirialize();
            while (true)
            {
                obj.Proccess();
            }
          
        }
       
    }
}
