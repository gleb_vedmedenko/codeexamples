﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web.Script.Serialization;
using Assets.Script;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
namespace ProjectServer
{
    public class ClientObj
    {
        public HttpListener listener;
        public static List<User> users = new List<User>();
        public static List<string> serializeUsers = new List<string>();
        public static readonly string path = string.Format("{0}{1}",
           Directory.GetCurrentDirectory(), "//Users.json");
        private enum ResponseEnum
        {
            WrongSignIn,
            SuccessRegister,
            SuccessSignIn,
            WrongRegister,
            GetData,
            NotEnoughtResource
        }
        private ResponseEnum responseType = new ResponseEnum();
        public ClientObj(HttpListener listener)
        {
            this.listener = listener;
            Console.WriteLine(path);
        }
        public void Proccess()
        {
            
            try
            {
                while (true)
                {
                    var context = listener.GetContext();
                   
                    var response = context.Response;

                    byte[] buffer = new byte[(int) context.Request.ContentLength64];
                    context.Request.InputStream.Read(buffer, 0, (int) context.Request.ContentLength64);
                    
                    if (buffer.Length > 0)
                    {
                        string json = Encoding.UTF8.GetString(buffer);
                        var user = new JavaScriptSerializer().Deserialize<User>(json);
                       
                        var feedBack = "";
                        if (user.Request == 0)
                        {
                            if (users.Any(x => x.Mail == user.Mail))
                            {
                                
                                responseType = ResponseEnum.WrongRegister;
                                feedBack = UserResponse(responseType, ref user);
                            }
                            else
                            {
                                user.ID = Guid.NewGuid().ToString();
                                var data = new UserData(0,0,0,1,user.ID,DateTime.Now);
                                user.userData = data;
                                users.Add(user);
                                responseType = ResponseEnum.SuccessRegister;
                                feedBack = UserResponse(responseType, ref user);
                            }
                        }
                        if (user.Request == 1)
                        {
                            if (users.Any(x => x.Mail == user.Mail && x.Password == user.Password))
                            {
                                var findUser = users.FirstOrDefault(x => x.Mail == user.Mail && x.Password == user.Password);   
                               
                                findUser.userData.UpdateResource();
                                responseType = ResponseEnum.SuccessSignIn;
                                feedBack = UserResponse(responseType, ref findUser);
                            }
                            else
                            {
                                
                                responseType = ResponseEnum.WrongSignIn;
                                feedBack = UserResponse(responseType, ref user);
                             
                            }
                        }
                        if (user.Request == 2)
                        {
                            var getUser = users.FirstOrDefault(x => x.ID == user.ID);

                            getUser.userData.UpdateResource();
                            if (getUser.userData.levelCoast > getUser.userData.Food)
                            {
                                responseType = ResponseEnum.NotEnoughtResource;
                                feedBack = UserResponse(responseType, ref getUser);
                                
                            }
                            else
                            {    
                                getUser.userData.levelUp();
                                Console.WriteLine(getUser.userData.LastUpdateDate);
                                responseType = ResponseEnum.GetData;
                                feedBack = UserResponse(responseType, ref getUser);
                            }

                        }
                        if (user.Name == "Stop")
                        {
                            listener.Stop();
                            Serialize();
                            break;
                        }
                        response.ContentLength64 = buffer.Length;
                        context.Response.ContentLength64 = Encoding.UTF8.GetByteCount(feedBack);
                        context.Response.StatusCode = (int) HttpStatusCode.OK;

                        using (Stream stream = context.Response.OutputStream)
                        {
                            using (StreamWriter writer = new StreamWriter(stream))
                            {
                                Console.WriteLine( feedBack);
                                writer.Write(feedBack);
                                writer.Flush();
                                writer.Close();
                            }
                        }
                    }   
                }
            }
            catch (HttpListenerException ex)
            {
                throw ex;
            }      
        }
        private string UserResponse(ResponseEnum responseType,ref User user)
        {
            string json = "";
            switch (responseType)
            {
                case ResponseEnum.SuccessRegister:
                     json = JsonConvert.SerializeObject(user);
                     return json;
                case ResponseEnum.SuccessSignIn:  
                     json = JsonConvert.SerializeObject(user);                
                     return json;
                case ResponseEnum.GetData:
                     json = JsonConvert.SerializeObject(user.userData);
                     return json;
                case ResponseEnum.WrongRegister:
                     return "1";
                case ResponseEnum.NotEnoughtResource:
                     return "2";
                case ResponseEnum.WrongSignIn:
                    return "0";
            }
            return "Unexeptable reason";
        }
       
        public  void Desirialize()
        {
            if (File.Exists(path))
            {
                using (StreamReader read = new StreamReader(path))
                {
                    string line;
                    
                    while ((line = read.ReadLine()) != null)
                    {
                        var user = new JavaScriptSerializer().Deserialize<User>(line);             
                        users.Add(user);
                    }
                }
            }
            else
            {
                File.Create(path);
            }
        }
        public void Serialize()
        {
            if (File.Exists(path))
            {
                using (StreamWriter writer = new StreamWriter(path))
                {
                    
                    foreach (var user in users)
                    {
                        string json = new JavaScriptSerializer().Serialize(user);
                        writer.WriteLine(json);
                    }
                }
            }
        }

    }
}
